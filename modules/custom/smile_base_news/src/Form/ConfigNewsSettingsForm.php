<?php

namespace Drupal\smile_base_news\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configures image toolkit settings for this site.
 */
class ConfigNewsSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_news_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config_news_form.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['header_page_news'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#format' => $this->config('config_news_form.settings')
        ->get('header_page_news.format'),
      '#default_value' => $this->config('config_news_form.settings')
        ->get('header_page_news.value'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('config_news_form.settings')
      ->set('header_page_news', $form_state->getValue('header_page_news'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
