<?php

namespace Drupal\smile_base_site\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'ContactUsBlock' block.
 *
 * @Block(
 *  id = "contact_us_block",
 *  admin_label = @Translation("Contact us block"),
 * )
 */
class ContactUsBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['contact_us_block'] = [
      '#theme' => 'smile_base_site_contact_us_block',
      '#title' => $this->t('Contact us'),
      '#url' => Url::fromRoute('entity.webform.canonical', [
        'webform' => 'contact',
      ]),
    ];
    return $build;
  }

}
