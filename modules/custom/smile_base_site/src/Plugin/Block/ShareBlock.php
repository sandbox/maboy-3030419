<?php

namespace Drupal\smile_base_site\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'ShareBlock' block.
 *
 * @Block(
 *  id = "share_block",
 *  admin_label = @Translation("Share block"),
 * )
 */
class ShareBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['smile_base_site_share_block'] = [
      '#theme' => 'smile_base_site_share_block',
      '#attached' => [
        'library' => [
          'smile_base_site/addthis',
        ],
      ],
    ];
    return $build;
  }

}
