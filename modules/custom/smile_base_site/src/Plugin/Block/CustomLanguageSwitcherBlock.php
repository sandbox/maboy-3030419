<?php

namespace Drupal\smile_base_site\Plugin\Block;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'CustomLanguageSwitcherBlock' block.
 *
 * @Block(
 *  id = "custom_language_switcher_block",
 *  admin_label = @Translation("Custom Language Switcher Block"),
 * )
 */
class CustomLanguageSwitcherBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $items = [];
    $language_manager = \Drupal::languageManager();
    $languages = $language_manager->getLanguages();
    if (count($languages) > 1) {
      $current_language = $language_manager->getCurrentLanguage()->getId();
      $links = $language_manager->getLanguageSwitchLinks("language_interface", Url::fromRoute('<current>'))->links;
      if (isset($links[$current_language])) {
        // Place active language ontop of list.
        $links = [$current_language => $links[$current_language]] + $links;
        // Set an active class for styling.
        $links[$current_language]['attributes']['class'][] = 'is-active';
      }
      foreach ($links as $item => $link) {
        // Join array elements with a string.
        $class = implode(' ', $link['attributes']['class']);
        /** @var \Drupal\language\Entity\ConfigurableLanguage $language */
        $language = $link['language'];
        /** @var \Drupal\Core\Url $url */
        $url = $link['url'];
        $query = $link['query'];
        $system_path = $url->getInternalPath();
        if (empty($system_path)) {
          $system_path = Url::fromRoute('<front>')->getUri();
        }
        $items[$item] = [
          'title' => $language->getName(),
          'id' => $language->getId(),
          'class' => $class,
          'system-path' => $system_path,
          'link-query' => Json::encode($query),
          'language' => $language,
        ];
      }
    }
    return [
      'custom_language_switcher_block' => [
        '#theme' => 'smile_base_theme_mobile_language_switcher',
        '#title' => $this->t('Language'),
        '#links' => $items,
        '#cache' => [
          'max-age' => 0,
        ],
      ],
    ];
  }

}
