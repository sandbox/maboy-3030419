<?php

namespace Drupal\smile_base_site\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\media\Entity\Media;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'FooterLogoBlock' block.
 *
 * @Block(
 *  id = "footer_logo_block",
 *  admin_label = @Translation("Footer logo block"),
 * )
 */
class FooterLogoBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * Construct.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    RendererInterface $renderer
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    $config_name = 'global';

    /* @var $config \Drupal\config_pages\Entity\ConfigPages */
    $config = config_pages_config($config_name);

    $list_cache_tags = $this->entityTypeManager->getDefinition('config_pages')->getListCacheTags();
    $build = [
      '#markup' => '',
      '#cache' => [
        'tags' => $list_cache_tags,
      ],
    ];

    if (!is_null($config)) {
      // Remove the list cache tag because a config page has been initiated.
      $build['#cache'] = [];
      // Add config cache dependency.
      $this->renderer->addCacheableDependency($build, $config);

      $target_id = $config->get('field_footer_logo')->getValue();
      if (!empty($target_id)) {
        $media = Media::load($target_id[0]['target_id']);
        /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $image_field */
        $image_field = $media->get('field_image');
        /** @var \Drupal\file\FileInterface[] $images */
        $images = $image_field->referencedEntities();
        if (!empty($images)) {
          $build['logo'] = [
            '#theme' => 'image_style',
            '#style_name' => 'large',
            '#uri' => $images[0]->getFileUri(),
          ];
          $this->renderer->addCacheableDependency($build['logo'], $media);
        }
      }
    }

    return $build;
  }

}
