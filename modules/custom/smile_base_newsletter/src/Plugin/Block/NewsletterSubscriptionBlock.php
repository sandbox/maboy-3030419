<?php

namespace Drupal\smile_base_newsletter\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'NewsletterSubscriptionBlock' block.
 *
 * @Block(
 *  id = "newsletter_subscription_block",
 *  admin_label = @Translation("Stay connected!"),
 * )
 */
class NewsletterSubscriptionBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'account_hash' => '',
      'mailing_list_domain' => '',
      'mailing_list_hash' => '',
      'anti_spam_token' => '',
      'head_line' => '',
      'description' => '',
      'placeholder' => $this->t('My email address'),
      'button_label' => $this->t("I'm subscribing"),
    ] + parent::defaultConfiguration();

  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form['account_hash'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Account hash'),
      '#description' => $this->t('Mailchimp account hash.'),
      '#default_value' => $this->configuration['account_hash'],
      '#required' => TRUE,
    ];
    $form['mailing_list_domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mailing list domain'),
      '#description' => $this->t('Mailchimp mailing list domain. Example: XXX.usX.list-manage.com'),
      '#default_value' => $this->configuration['mailing_list_domain'],
      '#required' => TRUE,
    ];
    $form['mailing_list_hash'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Mailing list hash'),
      '#description' => $this->t('Mailchimp mailing list hash.'),
      '#default_value' => $this->configuration['mailing_list_hash'],
      '#required' => TRUE,
    ];
    $form['anti_spam_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Anti-spam token'),
      '#description' => $this->t('Mailchimp anti-spam token'),
      '#default_value' => $this->configuration['anti_spam_token'],
      '#required' => TRUE,
    ];
    $form['head_line'] = [
      '#type' => 'textfield',
      '#title' => $this->t('head_line'),
      '#default_value' => $this->configuration['head_line'],
    ];
    $form['description'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Description'),
      '#format' => $this->configuration['description']['format'],
      '#default_value' => $this->configuration['description']['value'],
    ];
    $form['placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Placeholder'),
      '#default_value' => $this->configuration['placeholder'],
    ];
    $form['button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button label'),
      '#default_value' => $this->configuration['button_label'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['account_hash'] = $form_state->getValue('account_hash');
    $this->configuration['mailing_list_domain'] = $form_state->getValue('mailing_list_domain');
    $this->configuration['mailing_list_hash'] = $form_state->getValue('mailing_list_hash');
    $this->configuration['anti_spam_token'] = $form_state->getValue('anti_spam_token');
    $this->configuration['head_line'] = $form_state->getValue('head_line');
    $this->configuration['description'] = $form_state->getValue('description');
    $this->configuration['placeholder'] = $form_state->getValue('placeholder');
    $this->configuration['button_label'] = $form_state->getValue('button_label');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [
      '#theme' => 'smile_base_newsletter_subscription_block',
      '#account_hash' => $this->configuration['account_hash'],
      '#mailing_list_domain' => $this->configuration['mailing_list_domain'],
      '#mailing_list_hash' => $this->configuration['mailing_list_hash'],
      '#anti_spam_token' => $this->configuration['anti_spam_token'],
      '#head_line' => $this->configuration['head_line'],
      '#description' => $this->configuration['description'],
      '#placeholder' => $this->configuration['placeholder'],
      '#button_label' => $this->configuration['button_label'],
    ];

    return $build;
  }

}
