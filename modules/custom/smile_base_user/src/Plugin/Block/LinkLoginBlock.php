<?php

namespace Drupal\smile_base_user\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'LinkLoginBlock' block.
 *
 * @Block(
 *  id = "link_login_block",
 *  admin_label = @Translation("Link login block"),
 * )
 */
class LinkLoginBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['link_login_block'] = [
      '#title' => $this->t('Log in'),
      '#type' => 'link',
      '#url' => Url::fromRoute('user.page'),
    ];
    return $build;
  }

}
