<?php

namespace Drupal\smile_base_migrate\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\migrate\Event\MigrateEvents;
use Drupal\migrate\Event\MigratePostRowSaveEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class DefineHomepage.
 *
 * @package Drupal\smile_base_migrate
 */
class DefineHomepage implements EventSubscriberInterface {

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Key value factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[MigrateEvents::POST_ROW_SAVE] = ['setHomepage'];

    return $events;
  }

  /**
   * Function to react on migrate.post_row_save event.
   *
   * Set the homepage.
   *
   * @param \Drupal\migrate\Event\MigratePostRowSaveEvent $event
   *   The event.
   */
  public function setHomepage(MigratePostRowSaveEvent $event) {
    /** @var \Drupal\migrate\row $row */
    $row = $event->getRow();
    // The unique destination ID, as an array (accommodating multi-column keys),
    // of the item just imported.
    $destination_id_values = $event->getDestinationIdValues();
    $destination_id = array_shift($destination_id_values);

    $is_homepage = $row->getSourceProperty('is_homepage');

    if (!empty($is_homepage)) {
      $this->configFactory
        ->getEditable('system.site')
        ->set('page.front', '/node/' . $destination_id)
        ->save(TRUE);
    }
  }

}
