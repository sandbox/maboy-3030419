<?php

namespace Drupal\smile_base_search\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configures image toolkit settings for this site.
 */
class ConfigSearchSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_search_form_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config_search_form.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['header_text'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#format' => $this->config('config_search_form.settings')
        ->get('header_text.format'),
      '#default_value' => $this->config('config_search_form.settings')
        ->get('header_text.value'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('config_search_form.settings')
      ->set('header_text', $form_state->getValue('header_text'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
