<?php

namespace Drupal\smile_base_search\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'LinkSearchBlock' block.
 *
 * @Block(
 *  id = "link_search_block",
 *  admin_label = @Translation("Link search block"),
 * )
 */
class LinkSearchBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];
    $build['link_search_block'] = [
      '#title' => $this->t('Search'),
      '#type' => 'link',
      '#url' => Url::fromRoute('view.search.page_search'),
    ];
    return $build;
  }

}
