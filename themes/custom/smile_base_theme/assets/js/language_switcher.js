/**
 * @file
 * Contains the definition of the behavior language switcher.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Attaches the JS for the bootstrap tabcollapse library.
   */
  Drupal.behaviors.smile_base_theme_language_switcher = {
    attach: function (context) {
      $('.block-language', context)
        .once('smile_base_theme_language_switcher')
        .each(function () {
          $('.lang-wrapper', context).click(function () {
            $('ul', $(this)).toggleClass('active');
          });
          $('.lang-wrapper .is-active', context).click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            $('.links').toggleClass('active');
          });
        });
    }
  };

})(jQuery, Drupal);
