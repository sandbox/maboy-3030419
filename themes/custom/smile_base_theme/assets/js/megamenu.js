/**
 * @file
 * Contains the definition of the behavior language switcher.
 */

(function ($, Drupal) {

  'use strict';

  /**
   * Attaches the JS for the bootstrap tabcollapse library.
   */
  Drupal.behaviors.megamenu = {
    attach: function (context) {
      $('.menu-holder', context)
        .once('megamenu')
        .each(function () {
          $('.dropdown-toggle', context).click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this)
              .parent('li')
              .siblings('.expanded.expanded-opened')
              .removeClass('expanded-opened');
            $(this)
              .parent('li')
              .siblings('.expanded')
              .find('.menu-container')
              .slideUp();
            $(this)
              .parent('li')
              .toggleClass('expanded-opened')
              .find('.menu-container')
              .addClass('container-opened')
              .slideToggle();
            $(this).toggleClass('toggle-opened');
          });

          $('html', context).click(function (e) {
            $('.menu-container').slideUp();
            $('li').removeClass('expanded-opened');
          });

          $(window).on("load resize orientationchange", function () {
            if ($(window).width() <= 767) {
              $('ul li ul li.expanded  a', context).click(function (e) {
                e.preventDefault();
                e.stopPropagation();
                $(this).siblings('ul').slideToggle();
                $(this).parent('li').toggleClass('expanded-opened');
              });
            }
          });

          $('.hamburger', context).click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).toggleClass('is-active');
            $('.mobile-menu .mobile-collapse').slideToggle();
          });

          $('.menu-lang > li', context).click(function (e) {
            $(this).toggleClass('lang-opened');
            $('.langs').slideToggle();
          });
        });
    }
  };
})(jQuery, Drupal);
