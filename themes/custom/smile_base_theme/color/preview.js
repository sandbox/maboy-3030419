/**
 * @file
 * Preview for the Smile base theme.
 */
(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.color = {
    logoChanged: false,
    callback: function (context, settings, $form) {
      // Change the logo to be the real one.
      if (!this.logoChanged) {
        $('.color-preview .color-preview-logo img').attr('src', drupalSettings.color.logo);
        this.logoChanged = true;
      }
      // Remove the logo if the setting is toggled off.
      if (drupalSettings.color.logo === null) {
        $('div').remove('.color-preview-logo');
      }
      
      var $customPreview = $form.find('.custom_preview');
      var $colorPalette = $form.find('.js-color-palette');


      // text colors
      $customPreview.find('.primary-text-color').css('color', $colorPalette.find('input[name="palette[primary-text-color]"]').val());
      $customPreview.find('.secondary-text-color').css('color', $colorPalette.find('input[name="palette[secondary-text-color]"]').val());
      $customPreview.find('.tertiary-text-color').css('color', $colorPalette.find('input[name="palette[tertiary-text-color]"]').val());
      $customPreview.find('.quaternary-text-color').css('color', $colorPalette.find('input[name="palette[quaternary-text-color]"]').val());
      $customPreview.find('.newsletter-text-color').css('color', $colorPalette.find('input[name="palette[newsletter-text-color]"]').val());
      $customPreview.find('a').css('color', $colorPalette.find('input[name="palette[link]"]').val());

      // Backgrounds
      $customPreview.find('.primary-bg').css('background-color', $colorPalette.find('input[name="palette[primary-bg]"]').val());
      $customPreview.find('.secondary-bg').css('background-color', $colorPalette.find('input[name="palette[secondary-bg]"]').val());
      $customPreview.find('.tertiary-bg').css('background-color', $colorPalette.find('input[name="palette[tertiary-bg]"]').val());
      $customPreview.find('.quaternary-bg').css('background-color', $colorPalette.find('input[name="palette[quaternary-bg]"]').val());
      $customPreview.find('.quinary-bg').css('background-color', $colorPalette.find('input[name="palette[quinary-bg]"]').val());
      $customPreview.find('.senary-bg').css('background-color', $colorPalette.find('input[name="palette[senary-bg]"]').val());
      $customPreview.find('.septenary-bg').css('background-color', $colorPalette.find('input[name="palette[septenary-bg]"]').val());
      $customPreview.find('.octonary-bg').css('background-color', $colorPalette.find('input[name="palette[octonary-bg]"]').val());


      //buttons
      $customPreview.find('.primary-btn-bg').css('background-color', $colorPalette.find('input[name="palette[primary-btn-bg]"]').val());
      $customPreview.find('.primary-btn-bg').css('color', $colorPalette.find('input[name="palette[primary-btn-color]"]').val());
      $customPreview.find('.secondary-btn-bg').css('background-color', $colorPalette.find('input[name="palette[secondary-btn-bg]"]').val());
      $customPreview.find('.secondary-btn-bg').css('color', $colorPalette.find('input[name="palette[secondary-btn-color]"]').val());

    }
  };
})(jQuery, Drupal, drupalSettings);
