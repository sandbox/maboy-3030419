<?php

/**
 * @file
 * Lists available colors and color schemes for the Smile base theme.
 */

$info = [
  // Available colors and color labels used in theme.
  'fields' => [
    'primary-text-color' => t('Primary text color'),
    'secondary-text-color' => t('Secondary text color'),
    'tertiary-text-color' => t('Tertiary text color'),
    'quaternary-text-color' => t('Quaternary text color'),
    'newsletter-text-color' => t('Newsletter text color'),
    'primary-bg' => t('Primary background'),
    'secondary-bg' => t('Secondary background'),
    'tertiary-bg' => t('Tertiary background'),
    'quaternary-bg' => t('Quaternary background'),
    'quinary-bg' => t('Quinary background'),
    'senary-bg' => t('Senary background'),
    'septenary-bg' => t('Septenary background'),
    'octonary-bg' => t('Octonary background'),
    'primary-btn-bg' => t('Primary button background'),
    'primary-btn-color' => t('Primary button color'),
    'secondary-btn-bg' => t('Secondary button background'),
    'secondary-btn-color' => t('Secondary button color'),
    'link' => t('link'),
  ],
  // Pre-defined color schemes.
  'schemes' => [
    'default' => [
      'title' => t('Smile (default)'),
      'colors' => [
        'primary-text-color' => '#828282',
        'secondary-text-color' => '#FFFFFF',
        'tertiary-text-color' => '#3B80FF',
        'quaternary-text-color' => '#FF8054',
        'newsletter-text-color' => '#FFFFFF',
        'primary-bg' => '#397DFC',
        'secondary-bg' => '#FF8054',
        'tertiary-bg' => '#F1F1F1',
        'quaternary-bg' => '#FFFFFF',
        'quinary-bg' => '#8F8F8F',
        'senary-bg' => '#D2D2D2',
        'septenary-bg' => '#BEBEBE',
        'octonary-bg' => '#EEEEEE',
        'primary-btn-bg' => '#3B80FF',
        'primary-btn-color' => '#FFFFFF',
        'secondary-btn-bg' => '#FF8054',
        'secondary-btn-color' => '#FFFFFF',
        'link' => '#3B80FF',
      ],
    ],
  ],


  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => [
    'assets/css/colors.css',
  ],

  // Files to copy.
  'copy' => [
    'logo.png',
  ],

  // Gradient definitions.
  'gradients' => [
    [
      // (x, y, width, height).
      'dimension' => [0, 0, 0, 0],
      // Direction of gradient ('vertical' or 'horizontal').
      'direction' => 'vertical',
      // Keys of colors to use for the gradient.
      'colors' => ['top', 'bottom'],
    ],
  ],

  // Preview files.
  'preview_library' => 'smile_base_theme/color.preview',
  'preview_html' => 'color/preview.html',

  // Attachments.
  '#attached' => [
    'drupalSettings' => [
      'color' => [
        // Put the logo path into JavaScript for the live preview.
        'logo' => theme_get_setting('logo.url', 'smile_base_theme'),
      ],
    ],
  ],
];
